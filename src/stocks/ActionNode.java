package stocks;

public interface ActionNode extends Node
{
	public Action evaluate();
	public ActionNode copy();
}
