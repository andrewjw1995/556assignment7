package stocks;

public interface BooleanNode extends Node
{
	public boolean evaluate();
	public BooleanNode copy();
}
