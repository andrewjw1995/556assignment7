package stocks.nodes;

import java.util.*;

import stocks.*;

public class SumNode implements NumericNode
{
	public Input input;
	public NumericNode extent;

	public SumNode(Input input, NumericNode extent)
	{
		this.input = input;
		this.extent = extent;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public double evaluate()
	{
		List<Double> values = input.getRange((int) extent.evaluate());
		double sum = 0;
		for (double value : values)
			sum += value;
		return sum;
	}

	@Override
	public NumericNode copy()
	{
		return new SumNode(input, extent.copy());
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Arrays.asList(extent);
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
		extent = inputs.get(0);
	}
}
