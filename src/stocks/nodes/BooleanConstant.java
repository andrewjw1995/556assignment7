package stocks.nodes;

import java.util.*;

import stocks.*;

public class BooleanConstant implements BooleanNode
{
	public boolean value;

	public BooleanConstant(boolean value)
	{
		this.value = value;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public boolean evaluate()
	{
		return value;
	}

	@Override
	public BooleanNode copy()
	{
		return new BooleanConstant(value);
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
	}
}
