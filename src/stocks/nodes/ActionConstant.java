package stocks.nodes;

import java.util.*;

import stocks.*;

public class ActionConstant implements ActionNode
{
	public Action action;

	public ActionConstant(Action action)
	{
		this.action = action;
	}

	@Override
	public Action evaluate()
	{
		return action;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public ActionConstant copy()
	{
		return new ActionConstant(action);
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
	}
	
	@Override
	public String toString()
	{
		return new ToStringVisitor().getString(this);
	}
}
