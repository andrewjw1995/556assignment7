package stocks.nodes;

import java.util.*;

import stocks.*;

public class GreaterThanNode implements BooleanNode
{
	public NumericNode left;
	public NumericNode right;

	public GreaterThanNode(NumericNode left, NumericNode right)
	{
		this.left = left;
		this.right = right;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public boolean evaluate()
	{
		return left.evaluate() > right.evaluate();
	}

	@Override
	public BooleanNode copy()
	{
		return new GreaterThanNode(left.copy(), right.copy());
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Arrays.asList(left, right);
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
		left = inputs.get(0);
		right = inputs.get(1);
	}
}
