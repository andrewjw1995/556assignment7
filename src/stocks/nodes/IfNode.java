package stocks.nodes;

import java.util.*;

import stocks.*;

public class IfNode implements ActionNode
{
	public BooleanNode condition;
	public ActionNode pass;
	public ActionNode fail;

	public IfNode(BooleanNode predicate, ActionNode thenNode, ActionNode elseNode)
	{
		this.condition = predicate;
		this.pass = thenNode;
		this.fail = elseNode;
	}

	@Override
	public Action evaluate()
	{
		if (condition.evaluate())
		{
			return pass.evaluate();
		}
		else
		{
			return fail.evaluate();
		}
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public ActionNode copy()
	{
		return new IfNode(condition.copy(), pass.copy(), fail.copy());
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Arrays.asList(pass, fail);
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Arrays.asList(condition);
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
		pass = inputs.get(0);
		fail = inputs.get(1);
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
		condition = inputs.get(0);
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
	}
	
	@Override
	public String toString()
	{
		return new ToStringVisitor().getString(this);
	}
}
