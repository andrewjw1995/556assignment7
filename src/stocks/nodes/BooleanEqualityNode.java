package stocks.nodes;

import java.util.*;

import stocks.*;

public class BooleanEqualityNode implements BooleanNode
{
	public BooleanNode left;
	public BooleanNode right;

	public BooleanEqualityNode(BooleanNode left, BooleanNode right)
	{
		this.left = left;
		this.right = right;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public boolean evaluate()
	{
		return left.evaluate() == right.evaluate();
	}

	@Override
	public BooleanNode copy()
	{
		return new BooleanEqualityNode(left.copy(), right.copy());
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Arrays.asList(left, right);
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
		left = inputs.get(0);
		right = inputs.get(1);
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
	}
}
