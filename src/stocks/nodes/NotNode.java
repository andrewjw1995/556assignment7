package stocks.nodes;

import java.util.*;

import stocks.*;

public class NotNode implements BooleanNode
{
	public BooleanNode condition;

	public NotNode(BooleanNode negatedNode)
	{
		this.condition = negatedNode;
	}

	@Override
	public void visit(Visitor visitor)
	{
		visitor.visit(this);
	}

	@Override
	public boolean evaluate()
	{
		return !condition.evaluate();
	}

	@Override
	public BooleanNode copy()
	{
		return new NotNode(condition.copy());
	}

	@Override
	public List<ActionNode> getActionInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public List<BooleanNode> getBooleanInputs()
	{
		return Arrays.asList(condition);
	}

	@Override
	public List<NumericNode> getNumericInputs()
	{
		return Collections.emptyList();
	}

	@Override
	public void setActionInputs(List<ActionNode> inputs)
	{
	}

	@Override
	public void setBooleanInputs(List<BooleanNode> inputs)
	{
		condition = inputs.get(0);
	}

	@Override
	public void setNumericInputs(List<NumericNode> inputs)
	{
	}
}
