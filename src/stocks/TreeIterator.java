package stocks;

import java.util.function.Function;

import stocks.nodes.ActionConstant;
import stocks.nodes.AndNode;
import stocks.nodes.AverageNode;
import stocks.nodes.BooleanConstant;
import stocks.nodes.BooleanEqualityNode;
import stocks.nodes.GreaterEqualNode;
import stocks.nodes.GreaterThanNode;
import stocks.nodes.IfNode;
import stocks.nodes.LessEqualNode;
import stocks.nodes.LessThanNode;
import stocks.nodes.NotNode;
import stocks.nodes.NumericConstant;
import stocks.nodes.NumericEqualityNode;
import stocks.nodes.OrNode;
import stocks.nodes.SumNode;

public class TreeIterator implements Visitor
{
	static final TreeIterator SINGLETON = new TreeIterator(); 
	
	Function<Node,Void> action;
	
	public static synchronized void iterate(Function<Node,Void> action, Node root)
	{
		SINGLETON.action = action;
		root.visit(SINGLETON);
	}
	
	@Override
	public void visit(AndNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(AverageNode node)
	{
		action.apply(node);
		node.extent.visit(this);
	}

	@Override
	public void visit(BooleanConstant node)
	{
		action.apply(node);
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(ActionConstant node)
	{
		action.apply(node);
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(IfNode node)
	{
		action.apply(node);
		node.condition.visit(this);
		node.pass.visit(this);
		node.fail.visit(this);
	}

	@Override
	public void visit(LessEqualNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(LessThanNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(NotNode node)
	{
		action.apply(node);
		node.condition.visit(this);
	}

	@Override
	public void visit(NumericConstant node)
	{
		action.apply(node);
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(OrNode node)
	{
		action.apply(node);
		node.left.visit(this);
		node.right.visit(this);
	}

	@Override
	public void visit(SumNode node)
	{
		action.apply(node);
		node.extent.visit(this);
	}
}
