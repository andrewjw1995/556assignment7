package stocks;

import stocks.nodes.*;

public class PrintVisitor implements Visitor
{
	int depth = 0;
	int index = 0;
	
	public void print(String string)
	{
		StringBuilder padding = new StringBuilder(depth * 2);
		if (index < 10)
			padding.append(' ');
		padding.append(index);
		for (int i = 0; i < depth; i++)
			padding.append("  ");
		padding.append(string);
		System.out.println(padding);
	}
	
	@Override
	public void visit(AndNode node)
	{
		index++;
		depth++;
		print("AND");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(AverageNode node)
	{
		index++;
		depth++;
		print("AVERAGE");
		node.extent.visit(this);
		depth--;
	}
	
	@Override
	public void visit(BooleanConstant node)
	{
		index++;
		depth++;
		print("" + node.value);
		depth--;
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		index++;
		depth++;
		print("EQUAL TO");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(ActionConstant node)
	{
		index++;
		depth++;
		print(node.action.toString());
		depth--;
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		index++;
		depth++;
		print("GREATER EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		index++;
		depth++;
		print("GREATER THAN");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(IfNode node)
	{
		index++;
		depth++;
		print("IF");
		node.condition.visit(this);
		print("THEN");
		node.pass.visit(this);
		print("ELSE");
		node.fail.visit(this);
		depth--;
	}

	@Override
	public void visit(LessEqualNode node)
	{
		index++;
		depth++;
		print("LESS EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(LessThanNode node)
	{
		index++;
		depth++;
		print("LESS THAN");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(NotNode node)
	{
		index++;
		depth++;
		print("NOT");
		node.condition.visit(this);
		depth--;
	}

	@Override
	public void visit(NumericConstant node)
	{
		index++;
		depth++;
		print("" + node.value);
		depth--;
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		index++;
		depth++;
		print("EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(OrNode node)
	{
		index++;
		depth++;
		print("OR");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(SumNode node)
	{
		index++;
		depth++;
		print("SUM");
		node.extent.visit(this);
		depth--;
	}

}
