package stocks;

import java.util.function.Function;

public class Size implements Function<Node,Void>
{
	int size = 0;
	
	public int get()
	{
		return size;
	}

	@Override
	public Void apply(Node t)
	{
		size++;
		return null;
	}
}
