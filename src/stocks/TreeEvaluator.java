package stocks;

import java.util.function.BiFunction;

import org.opt4j.core.Objective.Sign;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class TreeEvaluator implements Evaluator<ActionNode>
{
	public static class MaxDepth implements BiFunction<Node,Integer,Void>
	{
		int max = 0;
		
		@Override
		public Void apply(Node node, Integer depth)
		{
			max = depth > max ? depth : max;
			return null;
		}
	};
	
	int maxDepth;
	Input input;
	
	@Inject
	public TreeEvaluator(@Named("Input") Input input, @Named("MaxDepth") int maxDepth)
	{
		this.input = input;
		this.maxDepth = maxDepth;
	}

	@Override
	public Objectives evaluate(ActionNode node)
	{
		int iterations = 1000;
		int length = 100;
		double money = 100;
		double investment = 0;
		for (int iteration = 0; iteration < iterations; iteration++)
		{
			input.setView(iteration, length);
			Action action = node.evaluate();
			
			switch(action)
			{
			case BUY:
				investment += money;
				money = 0;
				break;
			case SELL:
				money += investment;
				investment = 0;
				break;
			default:
				break;
			}
			
			double percent = input.normalized.get(iteration);
			investment = investment * percent;
		}
		double profit = money + investment;
		
		MaxDepth max = new MaxDepth();
		DepthVisitor.iterate(max, node);
		double depth = max.max;
		if (depth < 3)
			depth = 3; // Penalise trees with no conditional logic
		
		Objectives objectives = new Objectives();
		objectives.add("Profit", Sign.MAX, profit);
		objectives.add("Depth", Sign.MIN, depth);
		return objectives;
	}

}
