package stocks;

import java.util.Random;
import org.opt4j.core.problem.Creator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import stocks.nodes.*;

public class Generator implements Creator<TreeGenotype>
{
	public Random random;
	int depth = 0;
	int maxDepth;
	double scale;
	Input input;
	
	@Inject
	public Generator(@Named("MaxDepth") int maxDepth, @Named("NumericScale") double scale, @Named("Input") Input input)
	{
		this.random = new Random();
		this.maxDepth = maxDepth;
		this.scale = scale;
		this.input = input;
	}
	
	public ActionNode generateActionNode()
	{
		depth++;
		ActionNode node;
		
		if (depth >= maxDepth)
		{
			node = new ActionConstant(Action.values()[random.nextInt(Action.values().length)]);
		}
		else
		{
			switch(random.nextInt(2))
			{
			case 0:
				node = new ActionConstant(Action.values()[random.nextInt(Action.values().length)]);
				break;
			case 1:
				node = new IfNode(generateBooleanNode(), generateActionNode(), generateActionNode());
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
		depth--;
		return node;
	}
	
	public BooleanNode generateBooleanNode()
	{
		depth++;
		BooleanNode node;
		
		if (depth >= maxDepth)
		{
			switch(random.nextInt(5))
			{
			case 0:
				node = new LessThanNode(generateNumericNode(), generateNumericNode());
				break;
			case 1:
				node = new LessEqualNode(generateNumericNode(), generateNumericNode());
				break;
			case 2:
				node = new GreaterThanNode(generateNumericNode(), generateNumericNode());
				break;
			case 3:
				node = new GreaterEqualNode(generateNumericNode(), generateNumericNode());
				break;
			case 4:
				node = new NumericEqualityNode(generateNumericNode(), generateNumericNode());
				break;
			default:
				throw new IllegalStateException();
			}
		}
		else
		{
			switch(random.nextInt(9))
			{
			case 0:
				node = new AndNode(generateBooleanNode(), generateBooleanNode());
				break;
			case 1:
				node = new OrNode(generateBooleanNode(), generateBooleanNode());
				break;
			case 2:
				node = new NotNode(generateBooleanNode());
				break;
			case 3:
				node = new BooleanEqualityNode(generateBooleanNode(), generateBooleanNode());
				break;
			case 4:
				node = new LessThanNode(generateNumericNode(), generateNumericNode());
				break;
			case 5:
				node = new LessEqualNode(generateNumericNode(), generateNumericNode());
				break;
			case 6:
				node = new GreaterThanNode(generateNumericNode(), generateNumericNode());
				break;
			case 7:
				node = new GreaterEqualNode(generateNumericNode(), generateNumericNode());
				break;
			case 8:
				node = new NumericEqualityNode(generateNumericNode(), generateNumericNode());
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
		depth--;
		return node;
	}
	
	public NumericNode generateNumericNode()
	{
		depth++;
		NumericNode node;
		
		if (depth >= maxDepth)
		{
			node = new NumericConstant(random.nextDouble() * scale);
		}
		else
		{
			switch(random.nextInt(2))
			{
			case 0:
				node = new NumericConstant(random.nextDouble() * scale);
				break;
			case 1:
				node = new AverageNode(input, generateNumericNode());
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
		depth--;
		return node;
	}

	@Override
	public TreeGenotype create()
	{
		TreeGenotype genotype = new TreeGenotype();
		genotype.root(generateActionNode());
		return genotype;
	}
}
