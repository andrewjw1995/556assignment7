package stocks;

import java.util.function.Function;

import stocks.nodes.*;

public class SelectorVisitor implements Visitor
{
	Node selection;
	Function<Node, Boolean> selector;
	
	public Node select(Function<Node,Boolean> selector, Node root)
	{
		this.selection = null;
		this.selector = selector;
		root.visit(this);
		return selection;
	}

	@Override
	public void visit(AndNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(AverageNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.extent.visit(this);
	}

	@Override
	public void visit(BooleanConstant node)
	{
		if (selector.apply(node))
			selection = node;
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(ActionConstant node)
	{
		if (selector.apply(node))
			selection = node;
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(IfNode node)
	{
		if (selector.apply(node))
			selection = node;
		
		if (selection == null)
			node.condition.visit(this);
		if (selection == null)
			node.pass.visit(this);
		if (selection == null)
			node.fail.visit(this);
	}

	@Override
	public void visit(LessEqualNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(LessThanNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(NotNode node)
	{
		if (selector.apply(node))
			selection = node;
		
		if (selection == null)
			node.condition.visit(this);
	}

	@Override
	public void visit(NumericConstant node)
	{
		if (selector.apply(node))
			selection = node;
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(OrNode node)
	{
		if (selector.apply(node))
			selection = node;

		if (selection == null)
			node.left.visit(this);
		if (selection == null)
			node.right.visit(this);
	}

	@Override
	public void visit(SumNode node)
	{
		if (selector.apply(node))
			selection = node;
		
		if (selection == null)
			node.extent.visit(this);
	}
}
