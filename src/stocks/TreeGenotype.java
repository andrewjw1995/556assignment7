package stocks;

import org.opt4j.core.Genotype;

import stocks.nodes.ActionConstant;


public class TreeGenotype implements Genotype
{
	private ActionNode root = new ActionConstant(Action.BUY);
	private int size = -1;
	
	@Override
	@SuppressWarnings("unchecked")
	public TreeGenotype newInstance()
	{
		return new TreeGenotype();
	}

	@Override
	public int size()
	{
		if (size == -1)
		{
			Size sizer = new Size();
			TreeIterator.iterate(sizer, root);
			size = sizer.get();
		}
		return size;
	}
	
	public void root(ActionNode root)
	{
		this.root = root;
		this.size = -1;
	}
	
	public ActionNode root()
	{
		this.size = -1;
		return root;
	}
	
	public TreeGenotype copy()
	{
		TreeGenotype copy = new TreeGenotype();
		copy.root = root.copy();
		copy.size = -1;
		return copy;
	}
	
	@Override
	public String toString()
	{
		return new ToStringVisitor().getString(root);
	}
}
