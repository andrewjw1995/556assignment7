package stocks;

import java.io.IOException;
import java.nio.file.Paths;

import org.opt4j.core.Individual;
import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.problem.ProblemModule;
import org.opt4j.core.start.Opt4JTask;
import org.opt4j.optimizers.ea.EvolutionaryAlgorithmModule;
import org.opt4j.viewer.ViewerModule;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.name.Names;

import stocks.nodes.ActionConstant;
import stocks.operators.TreeCopyModule;
import stocks.operators.TreeCrossoverModule;
import stocks.operators.TreePointMutationModule;

public class TradingAgentModule extends ProblemModule
{
	protected void config()
	{
		bindProblem(Generator.class, TreeDecoder.class, TreeEvaluator.class);
	}
	
	public static void main(String[] args)
	{
		ViewerModule                viewer    = new ViewerModule();
		EvolutionaryAlgorithmModule algorithm = new EvolutionaryAlgorithmModule();
		Module                      operator1 = new TreeCrossoverModule();
		Module                      operator2 = new TreePointMutationModule();
		Module                      operator3 = new TreeCopyModule();
		TradingAgentModule          problem   = new TradingAgentModule();
		AbstractModule              constants;

		algorithm.setAlpha(25);              // The population size    
		algorithm.setLambda(25);             // The number of children 
		algorithm.setMu(2);                  // The number of parents  
		algorithm.setCrossoverRate(0.95);
		algorithm.setGenerations(500);
		int maxDepth = 4;
		
		TreeEvaluator debugger;
		try
		{
			Input input = Input.load(Paths.get("NZX50.csv"));
			debugger = new TreeEvaluator(input, maxDepth);
			constants = new AbstractModule(){
					@Override
					protected void configure() {
						bindConstant().annotatedWith(Names.named("NumericScale")).to(150.0);
						bindConstant().annotatedWith(Names.named("MaxDepth")).to(maxDepth);
						bind(Input.class).annotatedWith(Names.named("Input")).toInstance(input);
					}
				};
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}

		
		Opt4JTask task = new Opt4JTask(false);
		task.init(algorithm, operator1, operator2, operator3, problem, constants, viewer);
		
		try
		{
	        task.execute();
	        Archive archive = task.getInstance(Archive.class);
	        for (Individual individual : archive)
	        {
	        	ActionNode phenotype = (ActionNode)individual.getPhenotype();
	        	Objectives objectives = debugger.evaluate(phenotype);
	        	double profit = objectives.get(new Objective("Profit")).getDouble();
	        	profit -= debugger.evaluate(new ActionConstant(Action.BUY)).get(new Objective("Profit")).getDouble();
	        	phenotype.visit(new PrintVisitor());
	        	System.out.println(profit);
	        }
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
	        task.close();
		}
	}
}