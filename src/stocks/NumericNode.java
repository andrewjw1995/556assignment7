package stocks;

public interface NumericNode extends Node
{
	public double evaluate();
	public NumericNode copy();
}
