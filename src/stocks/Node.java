package stocks;

import java.util.List;

public interface Node
{
	public void visit(Visitor visitor);
	public List<ActionNode> getActionInputs();
	public void setActionInputs(List<ActionNode> inputs);
	public List<BooleanNode> getBooleanInputs();
	public void setBooleanInputs(List<BooleanNode> inputs);
	public List<NumericNode> getNumericInputs();
	public void setNumericInputs(List<NumericNode> inputs);
}
