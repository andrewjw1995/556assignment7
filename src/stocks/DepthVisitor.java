package stocks;

import java.util.function.BiFunction;

import stocks.nodes.ActionConstant;
import stocks.nodes.AndNode;
import stocks.nodes.AverageNode;
import stocks.nodes.BooleanConstant;
import stocks.nodes.BooleanEqualityNode;
import stocks.nodes.GreaterEqualNode;
import stocks.nodes.GreaterThanNode;
import stocks.nodes.IfNode;
import stocks.nodes.LessEqualNode;
import stocks.nodes.LessThanNode;
import stocks.nodes.NotNode;
import stocks.nodes.NumericConstant;
import stocks.nodes.NumericEqualityNode;
import stocks.nodes.OrNode;
import stocks.nodes.SumNode;

public class DepthVisitor implements Visitor
{
	public static final DepthVisitor SINGLETON = new DepthVisitor();
	
	int depth;
	BiFunction<Node,Integer,Void> action;
	
	public static synchronized void iterate(BiFunction<Node,Integer,Void> action, Node root)
	{
		SINGLETON.depth = -1;
		SINGLETON.action = action;
		root.visit(SINGLETON);
	}
	
	@Override
	public void visit(AndNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(AverageNode node)
	{
		depth++;
		action.apply(node, depth);
		node.extent.visit(this);
		depth--;
	}
	
	@Override
	public void visit(BooleanConstant node)
	{
		depth++;
		action.apply(node, depth);
		depth--;
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(ActionConstant node)
	{
		depth++;
		action.apply(node, depth);
		depth--;
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(IfNode node)
	{
		depth++;
		action.apply(node, depth);
		node.condition.visit(this);
		node.pass.visit(this);
		node.fail.visit(this);
		depth--;
	}

	@Override
	public void visit(LessEqualNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(LessThanNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(NotNode node)
	{
		depth++;
		action.apply(node, depth);
		node.condition.visit(this);
		depth--;
	}

	@Override
	public void visit(NumericConstant node)
	{
		depth++;
		action.apply(node, depth);
		depth--;
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(OrNode node)
	{
		depth++;
		action.apply(node, depth);
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(SumNode node)
	{
		depth++;
		action.apply(node, depth);
		node.extent.visit(this);
		depth--;
	}
}
