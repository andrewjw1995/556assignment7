package stocks;

import stocks.nodes.*;

public interface Visitor
{
	public void visit(AndNode node);
	public void visit(AverageNode node);
	public void visit(BooleanConstant node);
	public void visit(BooleanEqualityNode node);
	public void visit(ActionConstant node);
	public void visit(GreaterEqualNode node);
	public void visit(GreaterThanNode node);
	public void visit(IfNode node);
	public void visit(LessEqualNode node);
	public void visit(LessThanNode node);
	public void visit(NotNode node);
	public void visit(NumericConstant node);
	public void visit(NumericEqualityNode node);
	public void visit(OrNode node);
	public void visit(SumNode node);
}
