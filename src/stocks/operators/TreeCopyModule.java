package stocks.operators;

import org.opt4j.operators.copy.CopyModule;

public class TreeCopyModule extends CopyModule
{
	@Override
	protected void config()
	{
		addOperator(TreeCopyOperator.class);
	}
}
