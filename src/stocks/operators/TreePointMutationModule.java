package stocks.operators;

import org.opt4j.operators.mutate.MutateModule;

public class TreePointMutationModule extends MutateModule
{
	@Override
	protected void config()
	{
		addOperator(SelectivePointMutator.class);
	}
}
