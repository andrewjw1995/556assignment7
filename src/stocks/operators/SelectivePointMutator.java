package stocks.operators;

import java.util.Random;
import java.util.function.Function;

import org.opt4j.operators.mutate.Mutate;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import stocks.*;
import stocks.nodes.*;

public class SelectivePointMutator implements Mutate<TreeGenotype>, Visitor
{
	Generator generator;
	Random random;
	double scale;
	
	@Inject
	public SelectivePointMutator(Generator generator, @Named("NumericScale") double scale)
	{
		this.generator = generator;
		this.random = generator.random;
		this.scale = scale;
	}
	
	@Override
	public void mutate(TreeGenotype genotype, double p)
	{
		if (random.nextDouble() >= p)
			return;
		
		SelectorVisitor selector = new SelectorVisitor();
		int selection = random.nextInt(genotype.size() + 1);
		if (selection == genotype.size())
		{
			genotype.root(generator.generateActionNode());
			return;
		}
		
		Function<Node,Boolean> predicate = new Function<Node,Boolean>(){
			int index = -1;
			public Boolean apply(Node node){
				index++;
				return index == selection;
			}
		};
		Node node = selector.select(predicate, genotype.root());
		node.visit(this);
	}

	@Override
	public void visit(AndNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateBooleanNode();
		else
			node.right = generator.generateBooleanNode();
	}

	@Override
	public void visit(AverageNode node)
	{
		node.extent = generator.generateNumericNode();
	}

	@Override
	public void visit(BooleanConstant node)
	{
		node.value = !node.value;
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateBooleanNode();
		else
			node.right = generator.generateBooleanNode();
	}

	@Override
	public void visit(ActionConstant node)
	{
		node.action = Action.values()[random.nextInt(Action.values().length)];
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateNumericNode();
		else
			node.right = generator.generateNumericNode();
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateNumericNode();
		else
			node.right = generator.generateNumericNode();
	}

	@Override
	public void visit(IfNode node)
	{
		int x = random.nextInt(3);
		switch(x)
		{
		case 0:
			node.condition = generator.generateBooleanNode();
			break;
			
		case 1:
			node.pass = generator.generateActionNode();
			break;
			
		default:
			node.fail = generator.generateActionNode();
			break;
		}
	}

	@Override
	public void visit(LessEqualNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateNumericNode();
		else
			node.right = generator.generateNumericNode();
	}

	@Override
	public void visit(LessThanNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateNumericNode();
		else
			node.right = generator.generateNumericNode();
	}

	@Override
	public void visit(NotNode node)
	{
		node.condition = generator.generateBooleanNode();
	}

	@Override
	public void visit(NumericConstant node)
	{
		node.value = random.nextDouble() * scale;
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateNumericNode();
		else
			node.right = generator.generateNumericNode();
	}

	@Override
	public void visit(OrNode node)
	{
		if (random.nextBoolean())
			node.left = generator.generateBooleanNode();
		else
			node.right = generator.generateBooleanNode();
	}

	@Override
	public void visit(SumNode node)
	{
		node.extent = generator.generateNumericNode();
	}
}
