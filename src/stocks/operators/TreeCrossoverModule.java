package stocks.operators;

import org.opt4j.operators.crossover.CrossoverModule;

public class TreeCrossoverModule extends CrossoverModule
{
	@Override
	protected void config()
	{
		addOperator(SelectiveCrossoverMutator.class);
	}
}
