package stocks.operators;

import java.util.*;
import java.util.function.Function;

import org.opt4j.operators.crossover.Crossover;
import org.opt4j.operators.crossover.Pair;

import stocks.*;

public class SelectiveCrossoverMutator implements Crossover<TreeGenotype>
{
	Random random = new Random();

	@Override
	public Pair<TreeGenotype> crossover(TreeGenotype a, TreeGenotype b)
	{
		TreeGenotype copyA = a.copy();
		TreeGenotype copyB = b.copy();
		
		int selectionIndex = random.nextInt(copyA.size());
		Function<Node,Boolean> predicate = new Function<Node,Boolean>(){
			int index = -1;
			public Boolean apply(Node node){
				index++;
				return index == selectionIndex;
			}
		};
		SelectorVisitor selector = new SelectorVisitor();
		Node selectionA = selector.select(predicate, copyA.root());

		if (selectionA == null)
			return new Pair<TreeGenotype>(copyA, copyB);
		
		final List<Node> matches = new ArrayList<>();
		TreeIterator.iterate((node) -> {
			if ((selectionA.getBooleanInputs().size() > 0 && node.getBooleanInputs().size() > 0) ||
			(selectionA.getNumericInputs().size() > 0 && node.getNumericInputs().size() > 0) ||
			(selectionA.getActionInputs().size() > 0 && node.getActionInputs().size() > 0))
				matches.add(node);
			return null;
		}, copyB.root());
		
		if (matches.size() == 0)
			return new Pair<TreeGenotype>(copyA, copyB);
		
		Node selectionB = matches.get(random.nextInt(matches.size()));
		if (selectionB == null)
			return new Pair<TreeGenotype>(copyA, copyB);
		
		List<ActionNode> actionInputsA = selectionA.getActionInputs();
		List<ActionNode> actionInputsB = selectionB.getActionInputs();
		List<BooleanNode> booleanInputsA = selectionA.getBooleanInputs();
		List<BooleanNode> booleanInputsB = selectionB.getBooleanInputs();
		List<NumericNode> numericInputsA = selectionA.getNumericInputs();
		List<NumericNode> numericInputsB = selectionB.getNumericInputs();
		if (actionInputsA.size() > 0 && actionInputsB.size() > 0)
		{
			int indexA = random.nextInt(actionInputsA.size());
			int indexB = random.nextInt(actionInputsB.size());
			ActionNode swap = actionInputsA.get(indexA);
			actionInputsA.set(indexA, actionInputsB.get(indexB));
			actionInputsB.set(indexB, swap);
			selectionA.setActionInputs(actionInputsA);
			selectionB.setActionInputs(actionInputsB);
		}
		else if (booleanInputsA.size() > 0 && booleanInputsB.size() > 0)
		{
			int indexA = random.nextInt(booleanInputsA.size());
			int indexB = random.nextInt(booleanInputsB.size());
			BooleanNode swap = booleanInputsA.get(indexA);
			booleanInputsA.set(indexA, booleanInputsB.get(indexB));
			booleanInputsB.set(indexB, swap);
			selectionA.setBooleanInputs(booleanInputsA);
			selectionB.setBooleanInputs(booleanInputsB);
		}
		else
		{
			int indexA = random.nextInt(numericInputsA.size());
			int indexB = random.nextInt(numericInputsB.size());
			NumericNode swap = numericInputsA.get(indexA);
			numericInputsA.set(indexA, numericInputsB.get(indexB));
			numericInputsB.set(indexB, swap);
			selectionA.setNumericInputs(numericInputsA);
			selectionB.setNumericInputs(numericInputsB);
		}
		return new Pair<TreeGenotype>(copyA, copyB);
	}

}