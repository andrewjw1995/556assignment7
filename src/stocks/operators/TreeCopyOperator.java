package stocks.operators;

import org.opt4j.operators.copy.Copy;

import stocks.TreeGenotype;

public class TreeCopyOperator implements Copy<TreeGenotype>
{
	@Override
	public TreeGenotype copy(TreeGenotype genotype)
	{
		return genotype.copy();
	}
}
