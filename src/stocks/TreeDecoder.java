package stocks;

import org.opt4j.core.problem.Decoder;

public class TreeDecoder implements Decoder<TreeGenotype, ActionNode>
{
	@Override
	public ActionNode decode(TreeGenotype genotype)
	{
		return genotype.root();
	}
}
