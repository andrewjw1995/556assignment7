package stocks;

import org.opt4j.core.Genotype;
import org.opt4j.operators.copy.Copy;

import stocks.nodes.ActionConstant;
import stocks.nodes.AndNode;
import stocks.nodes.AverageNode;
import stocks.nodes.BooleanConstant;
import stocks.nodes.BooleanEqualityNode;
import stocks.nodes.GreaterEqualNode;
import stocks.nodes.GreaterThanNode;
import stocks.nodes.IfNode;
import stocks.nodes.LessEqualNode;
import stocks.nodes.LessThanNode;
import stocks.nodes.NotNode;
import stocks.nodes.NumericConstant;
import stocks.nodes.NumericEqualityNode;
import stocks.nodes.OrNode;
import stocks.nodes.SumNode;

public class TreeCopy implements Copy<TreeGenotype> {

	private class CopyVisitor implements Visitor {
		TreeGenotype genotype = new TreeGenotype();
		@Override
		public void visit(AndNode node) {
			
			
		}

		@Override
		public void visit(AverageNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(BooleanConstant node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(BooleanEqualityNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(ActionConstant node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(GreaterEqualNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(GreaterThanNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(IfNode node) {
			
			
		}

		@Override
		public void visit(LessEqualNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(LessThanNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(NotNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(NumericConstant node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(NumericEqualityNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(OrNode node) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(SumNode node) {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public TreeGenotype copy(TreeGenotype arg0) {
		// TODO Auto-generated method stub
		return new TreeGenotype();
	}

}
