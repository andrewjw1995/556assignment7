package stocks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Input
{
	List<Double> values;
	List<Double> normalized;
	int offset;
	int length;
	
	public Input()
	{
		this.values = new ArrayList<>();
	}
	
	public Input(List<Double> values, List<Double> normalized)
	{
		this.values = values;
		this.normalized = normalized;
	}
	
	public void setView(int offset, int length)
	{
		if (offset + length > normalized.size())
			throw new IndexOutOfBoundsException("The value of offset + length must be less than the total number of values");
		this.offset = offset;
		this.length = length;
	}
	
	public List<Double> getRange(int extent)
	{
		int end = offset + length;
		int start = end - extent;
		if (start < 0)
			start = 0;
		return normalized.subList(start, end);
	}
	
	public static Input load(Path path) throws IOException
	{
		List<Double> values = Files.lines(path)
			.skip(1)
			.map(line -> Double.parseDouble(line.split(",")[1]))
			.collect(Collectors.toList());
		Collections.reverse(values);
		
		List<Double> normalized = values.stream()
			.map(new Function<Double,Double>(){
				Double previous = null;
				@Override
				public Double apply(Double current)
				{
					if (previous == null)
					{
						previous = current;
						return null;
					}
					
					double increase = current / previous;
					previous = current;
					return increase;
				}
			})
			.skip(1)
			.collect(Collectors.toList());
		return new Input(values, normalized);
	}
}
