package stocks;

import stocks.nodes.*;

public class ToStringVisitor implements Visitor
{
	int depth = 0;
	StringBuilder builder;
	
	public String getString(Node node)
	{
		builder = new StringBuilder();
		node.visit(this);
		return builder.toString();
	}
	
	public void print(String string)
	{
		for (int i = 0; i < depth; i++)
			builder.append("  ");
		builder.append(string);
		builder.append('\n');
	}
	
	@Override
	public void visit(AndNode node)
	{
		depth++;
		print("AND");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(AverageNode node)
	{
		depth++;
		print("AVERAGE");
		node.extent.visit(this);
		depth--;
	}
	
	@Override
	public void visit(BooleanConstant node)
	{
		depth++;
		print("" + node.value);
		depth--;
	}

	@Override
	public void visit(BooleanEqualityNode node)
	{
		depth++;
		print("EQUAL TO");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(ActionConstant node)
	{
		depth++;
		print(node.action.toString());
		depth--;
	}

	@Override
	public void visit(GreaterEqualNode node)
	{
		depth++;
		print("GREATER EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(GreaterThanNode node)
	{
		depth++;
		print("GREATER THAN");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(IfNode node)
	{
		depth++;
		print("IF");
		node.condition.visit(this);
		print("THEN");
		node.pass.visit(this);
		print("ELSE");
		node.fail.visit(this);
		depth--;
	}

	@Override
	public void visit(LessEqualNode node)
	{
		depth++;
		print("LESS EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(LessThanNode node)
	{
		depth++;
		print("LESS THAN");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(NotNode node)
	{
		depth++;
		print("NOT");
		node.condition.visit(this);
		depth--;
	}

	@Override
	public void visit(NumericConstant node)
	{
		depth++;
		print("" + node.value);
		depth--;
	}

	@Override
	public void visit(NumericEqualityNode node)
	{
		depth++;
		print("EQUAL");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(OrNode node)
	{
		depth++;
		print("OR");
		node.left.visit(this);
		node.right.visit(this);
		depth--;
	}

	@Override
	public void visit(SumNode node)
	{
		depth++;
		print("SUM");
		node.extent.visit(this);
		depth--;
	}

}
